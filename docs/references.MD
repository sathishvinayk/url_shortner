## References

### Source Code
https://gitlab.biw-services.com/gts/url-shortener.git

### Package
Language +> Javascript
Framework +> Node
DB +> NoSql

### Deployments
Yet to be deployed.