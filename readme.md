# url shortner service
Service to shorten long/moderate url and provide a elegant short url.

For example:
Long run => `https://www.amazon.com/b/ref=AIS_GW_deals?node=15529609011&pf_rd_p=d8c15424-57f7-4e00-b2ca-4ef35edc015e&pf_rd_r=3AN5P65FA48PT0K2JKXC`

shortened => `http://dev.server.com/asg12D`

## Getting started
Below Instructions will get the code up and running on your local machine for development and testing purpposes.

### Features
This project is solely merged with ES6 code. Node experimental modules for es6 is provided for the future proof. Transpilation is done with the help of Babel.

### Prerequisites
Some of the platform marked below is in needed to run it. Please rename .env.xam to .env to get the env variables.

```
node >= v10.13.0
mongo >= 3.2
npm >= 6
Yarn @1.13.0
nodemon@latest installed globally ( Not mandatory )
MongoDb Compass (View for the DB) ( Not mandatory )(Can use anything of your to access Mongo docs)
```

### Installing
Clone the repo and install the dependencies with 

```
yarn install --save
```

### Running
By default project starts with Nodemon(dev-server) by giving the cmd as

```
yarn start 
```

Before starting the application, please make sure to run the Mongo Server.

### Logging
Http rotational Logging is taken with the help of `rotating-file-stream` with 1 day stored on file.
For any changes to add it on the db will be done while deployment.

### Running tests
Tests is under seperate tests folder to avoid devDepencies on the main project. Run the test on new terminal by navigating into tests folder and run 

```
yarn install --save
```

Start the test with 

```
yarn run test-http ( Shortner service should be running )
yarn run test-model
```