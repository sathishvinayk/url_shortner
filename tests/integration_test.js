'use strict'

require = require("esm")(module);

const chai = require("chai");
const chaiHttp = require('chai-http');
const chaiUrl = require('chai-url');
var chaiXml = require('chai-xml');
const request = require("superagent");
const assert = chai.assert;
const xml2js = require('xml2js').parseString;

chai.use(chaiHttp);
chai.use(chaiUrl);
chai.use(chaiXml);
const version = "v1"

const URI = process.env.SERVER_URL +'/'+ version;

describe("Integration http test", () => {
    describe('Get /',  () => {
        it("should return status 200", async function() {
            let data = await request.get(URI + '/');            
            chai.expect(data.res).to.have.status(200)
            chai.expect(data.res).to.be.json;
            assert.deepEqual(data.body, {ping : 'success'})
        })
    })
    describe('Post /shorten json output', () => {
        //Need to wrap more here
        it("should return a json output(create one if not present)", async function(){
            let data = await request.post(URI + '/shorten')
                .type('form')
                .send({url: 'https://marvals.com/15212/products11'})
                .send({title: 'Marvels'})
                .send({output: 'json'})
            assert.exists(data);
            chai.expect(data.res).to.have.status(200)
            chai.expect(data.res).to.be.json;
            assert.deepEqual(data.body.title, 'Marvels')
            assert.deepEqual(data.body.url, 'https://marvals.com/15212/products11')
        })
    })
    describe('Post /shorten xml output', () => {
        it("should return a xml output(create one if not present)", async function(){
            let data = await request.post(URI + '/shorten')
                .type('form')
                .send({url: 'https://marvals.com/15212/products11'})
                .send({title: 'Marvels'})
                .accept('text/xml')
            assert.exists(data);
            chai.expect(data.res).to.have.status(200)
            chai.expect(data.res.text).xml.to.be.valid()
            xml2js(data.res.text, (err, result) => {
                if(err) return console.error(err)
                assert.deepEqual(result.result.url[0].title[0], 'Marvels')
            })
        })
        it("should return error if url is invalid with 422", async function() {
            try {
                await request.post(URI + '/shorten')
                    .type('form')
                    .send({url: '115124512asdat'})
                    .send({title: 'some invalid title'})
                    .accept('text/xml')
            } catch(e){
                let data = e.response;
                chai.expect(e).to.have.status(422)
                chai.expect(data.res.text).xml.to.be.valid()
                assert.deepEqual(data.res.statusMessage, 'Unprocessable Entity')
            }
        })
        it("should return error if url is missing/empty with 400", async function() {
            try {
                await request.post(URI + '/shorten')
                    .type('form')
                    .send({url: ''})
                    .send({title: 'some invalid title'})
                    .accept('text/xml')
            } catch(e){
                let data = e.response;
                chai.expect(data).to.have.status(400)
                chai.expect(data.res.text).xml.to.be.valid()
                assert.deepEqual(data.res.statusMessage, 'Bad Request')
            }
        })
        it("should return error if url is passed as array with 400", async function() {
            try {
                await request.post(URI + '/shorten')
                    .type('form')
                    .send({url: ['https://marvals.com/15212/products11', '115124512asdat']})
                    .send({title: 'some invalid title'})
                    .accept('text/xml')
            } catch(e){
                let data = e.response;
                chai.expect(data).to.have.status(400)
                chai.expect(data.res.text).xml.to.be.valid()
                assert.deepEqual(data.res.statusMessage, 'Bad Request')
            }
        })      
    })
    describe('Redirect url must be proper', function() {
        it("should redirect to the original url and text should be equal as original url", async function() {
            let longurl = 'http://developers.jollypad.com/fb/index.php?dmmy=1&fb_sig_in_iframe=1&fb_sig_iframe_key=8e296a067a37563370ded05f5a3bf3ec&fb_sig_locale=bg_BG&fb_sig_in_new_facebook=1&fb_sig_time=1282749119.128&fb_sig_added=1&fb_sig_profile_update_time=1229862039&fb_sig_expires=1282755600&fb_sig_user=761405628&fb_sig_session_key=2.IuyNqrcLQaqPhjzhFiCARg__.3600.1282755600-761405628&fb_sig_ss=igFqJKrhJZWGSRO__Vpx4A__&fb_sig_cookie_sig=a9f110b4fc6a99db01d7d1eb9961fca6&fb_sig_ext_perms=user_birthday,user_religion_politics,user_relationships,user_relationship_details,user_hometown,user_location,user_likes,user_activities,user_interests,user_education_history,user_work_history,user_online_presence,user_website,user_groups,user_events,user_photos,user_videos,user_photo_video_tags,user_notes,user_about_me,user_status,friends_birthday,friends_religion_politics,friends_relationships,friends_relationship_details,friends_hometown,friends_location,friends_likes,friends_activities,friends_interests,friends_education_history,friends_work_history,friends_online_presence,friends_website,friends_groups,friends_events,friends_photos,friends_videos,friends_photo_video_tags,friends_notes,friends_about_me,friends_status&fb_sig_country=bg&fb_sig_api_key=9f7ea9498aabcd12728f8e13369a0528&fb_sig_app_id=177509235268&fb_sig=1a5c6100fa19c1c9b983e2d6ccfc05ef'
            let data = await request.post(URI + '/shorten')
                .type('form')
                .send({url: longurl})
                .send({title: 'jollypad'})
                .send({output: 'json'})
                console.log(data)
            let req = await request.get(data.body.shorturl);
            console.log(req)
            chai.expect(req.redirects[0]).to.equal(longurl)
        })
    })
})