'use strict'

require = require("esm")(module);
const chai = require("chai");
const chaiUrl = require('chai-url');
const assert = chai.assert;
const model = require("../src/models");

chai.use(chaiUrl);

describe("DB model test", function() {
    beforeEach(async function() {
        try {
            const urlkeyz = await model.urllist();
            for (let key in urlkeyz){
                await model.destroy(key);
            }
            await model.create("http://dev.service.com/tWwQUcY", "http://google.com/product1/161/cloud", "Google Product", "::1", "tWwQUcY");
            await model.create("http://prod.service.co/1AgaaGR", "http://amazon.com/product1/151/accesories", "Amazon Product", "::1", "1AgaaGR");
            await model.create("http://pprd.enterprise.com/161712A", "http://microsoft.com/product1/151/micro", "Microsoft Product", "::1", "161712A");
        } catch(e){
            console.log("testing error", e)
            throw e;
        }
    })
    describe("check urllist", function() {
        it("should have three entries", async function() {
            const list = await model.urllist();
            assert.exists(list);
            assert.isArray(list);
            assert.lengthOf(list, 3)
        });
        it("should have shorturl as 'http://dev.service.com/tWwQUcY', 'http://prod.service.co/1AgaaGR', 'http://pprd.enterprise.com/161712A'", async function() {
            const list = await model.urllist();
            assert.exists(list);
            assert.isArray(list);
            assert.lengthOf(list, 3)
            chai.expect(list[0]).to.have.path('/tWwQUcY');
            chai.expect(list[1]).to.have.path('/1AgaaGR');
            chai.expect(list[2]).to.have.path('/161712A');
        });
    });
    describe("read url data", function(){
        it("should have proper url", async function(){
            const doc = await model.read("tWwQUcY")
            assert.exists(doc);
            assert.deepEqual({
                ip: doc.ip, keyword:doc.keyword, url: doc.url, title: doc.title, shorturl: doc.shorturl, 
            }, {
                ip: "::1",
                keyword: "tWwQUcY",
                url: "http://google.com/product1/161/cloud",
                title: "Google Product",
                shorturl: "http://dev.service.com/tWwQUcY"
            });
        });
        it("unknown url should fail", async function(){
            try {
                const doc = await model.read("invalidkeymasnmn.com/6151612")
                assert.notExists(doc);
                throw new Error("control Should not get here")
            }catch(err) {
                assert.notEqual(err.message, "control should not get here")
            }
        });
    });
    afterEach(function() {
        //console.log('AfterEach')
    })
    after(function() {
        model.close();
    })
})