FROM node:10

RUN mkdir -p /usr/src/url-shortner
WORKDIR /usr/src/url-shortner
COPY package.json yarn.lock /usr/src/url-shortner/

RUN yarn install

COPY . /usr/src/url-shortner

EXPOSE 3000
CMD ["yarn", "start"]