import express from 'express';
import path from 'path';
import logger from 'morgan';
import bodyParser from 'body-parser';
import util from 'util';
import DBG from 'debug';
import dotenv from 'dotenv';
import fs from 'fs-extra';
import { xmlerror } from './helpers';

// Importing Env var here
dotenv.config()

// Error handler
const error = DBG('url-shortner:error');

// Routes 
// import { router as index } from './routes/index';
import { router as index } from './routes/v1/index'
import { router as shorten } from './routes/v1/shorten';

// Workaround for lack of __dirname in ES6 modules. For more info, please refer to dirname.js
import dirname from './dirname.js'
const { __dirname } = dirname;

const app = express();

/*
* ROTATION LOGS
* LogStream performs rotational file stream upto 10M
* Compressed => Yes
*/
import rfs from 'rotating-file-stream';
let logDirectory = path.join(__dirname, 'logs')
fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory)

var accessStream = rfs('access.log', {
  size: '10M', // Rotate every 10MB Written
  interval: '1d', //Rotate daily
  compress: 'gzip',  //Compress rotated files
  path: logDirectory
});
app.disable('etags');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger(process.env.REQUEST_LOG_FORMAT || 'dev', {
  stream: process.env.LOG_PROD_MODE ? accessStream : process.stdout
}));
app.use(bodyParser.json());
app.disable('x-powered-by');
app.use(bodyParser.urlencoded({ extended: false }));

//Routes
app.use('/v1/', index);
app.use('/v1/shorten', shorten);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

/**
 * Handle unhandledException 
 */
process.on('unhandledException', (err) => {
  error("App has crashed!!! - " + (err.stack || err));
})

/**
 * Handle unhandledRejection 
 */
process.on('unhandledRejection', (reason, p) => {
  error(`Unhandled Rejection at: ${util.inspect(p)} reason: ${reason}`);
})

if(app.get('env') === 'development'){
  app.use(function(err, req,res, next) {
    res.status(err.status || 500);
    error((err.status || 500) + ' ' + error.message);
    res.json({
      "status": err.Error,
      "message": err.message
    })
  });
}

// error handler -- Check env to production so that error will be shown as xml
app.use(function(err, req, res, next) {
  res.setHeader('content-type', 'text/xml');
  res.status(err.status || 500);

  let message = xmlerror(err.message)
  res.send(message);
});

export default app;
