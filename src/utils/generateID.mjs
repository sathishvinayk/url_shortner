/**
 * Maximum combination of id generator is placed here
 * Take AlphaNumeric and length of short_id's and math powers it to 
 * give ~800 Million Combination
 */

const ALPHANUMERICALS = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMONPQRSTUVWXYZ';
const LENGTH = ALPHANUMERICALS.length;
const SHORT_ID_LENGTH = 7
const MAXIMUM_IDS = Math.pow(LENGTH, SHORT_ID_LENGTH)

/**
 * RandomID
 * @params {null}
 * @returns {number} id
 */
function randomID() {
    let id = '';
    for(let i = 0; i < SHORT_ID_LENGTH; ++i){
        id += ALPHANUMERICALS.charAt(Math.floor(Math.random() * LENGTH));
    }
    
return id;
}

/**
 * Arbitary Generator fn
 * @params {null}
 * @returns {string} alphanumberic 
 */
export default function *arbitrary() {
    let ids = new Set();
    let i = 0;
    while(++i < MAXIMUM_IDS) {
        let id = randomID();
        while(ids.has(id)){
            id = randomID();
        }
        ids.add(id)
        yield id;
    }
}
