/**
 * Validates with Node url Validator for checking the url passed out
 * @param {string} url (original url)
 * @returns {string} href extracted from url
 */
export async function checkurl(url){    
    try { 
        let verfiedUrl = await new URL(url);    
        
        return verfiedUrl.href;
    } catch(e) {
        if(e instanceof TypeError){
            throw new Error(e.name, e.message)
        } else {
            throw new Error(e.name, e.message)
        }
    }
}
