/**
 * Class Strip 
 * @Params(generateID) For generating values
 * Method Async create()
 * Method Async getURL()
 */
export default class StripUrl {
    constructor(generateID){
        this.gen = generateID();
    }

    async getid(){
        let id = await this.gen.next().value;
 
        return id;
    }
}
