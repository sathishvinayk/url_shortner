/**
 * Global module for storage, keeping this as generic for future connectivity
 */
var urlModule = null;

/**
 * Sync load DB model
 * @params {null}
 * @returns {obj}Urlmodule
 */
async function model() {
    if(urlModule) {

        return urlModule; 
    }    
    urlModule = await import(`../db`);
    
    return urlModule;
}

/** 
 * @param {string} host shorturl 
 * @param {string} url long url
 * @param {string} title from request
 * @param {string} ip from request
 * @param {string} keyword generated for the url
 * @returns {string} with data
 */
export async function create(host, url, title, ip, keyword) {
 return (await model()).create(host, url, title, ip, keyword) 
}

/**
 * Destroy model
 * @param {string} key Alphanumeric generated keyword 
 * @returns {object}
 */
export async function destroy(key) {
 return (await model()).destroy(key);
}

/**
 * Destroy model
 * @params {null}
 * @returns {object}
 */
export async function urllist() {
 return (await model()).urllist(); 
}

/**
 * Read model
 * @param {string} key (keyword)
 * @returns {object}
 */
export async function read(key){
 return (await model()).read(key); 
}

/**
 * Close model
 * @params {null}
 * @returns {object}
 */
export async function close() {
 return (await model()).close(); 
}
