import * as model from '../models';
import strip from '../utils/stripURL.mjs';
import arbitrary from '../utils/generateID.mjs';
import { checkurl as urlvalidator } from '../utils/urlValidator.mjs';
import { ErrorHandler } from '../helpers'

/**
 * Asynchronously loads the strip class.
 * Initiating new instance of strip url to pass generated values
 * Generator for the generateID yields new values
 */
let STRIP = new strip(arbitrary);

/**
 * Async bounce for redirection.Validation for accepting only (a-zA-Z0-9) provided.
 * @param {string}id(Short-id Key)
 * @Returns {obj} doc
 */
export async function bounce(id){    
    let shortid = id;
    let checkid = (/[^\w]|_/g).test(shortid);
    if (checkid) {
        throw new ErrorHandler(400, "Keyword provided is invalid")
    }
    if(shortid.length > 7 || shortid.length < 7) {
        throw new ErrorHandler(400, "Please provide a valid url")
    }
    try {
        
        return await model.read(shortid); 
    }catch(e){
        throw e;
    }
}

/**Short id generator
 * @params {null}
 * @Returns {object} shorturl and id
 */
async function createshort(){
    let URI = process.env.DEV_URL,
        host = {},
        hostname = Symbol("hostname");
        
    try {
        let id = await STRIP.getid();   
        host[hostname] = URI + "/" + id;
        let shorturl = host[hostname]

        return {
            shorturl,
            id
        }
    } catch(e){ 
        throw e
    }
}

/**
 * Shorten fn
 * @param {string }ip (url straight from req.body)
 * @param {string }url (url straight from req.body)
 * @param {title}title (title straight from req.body)
 * @Returns {object} document
 * Get the generated ID from the strip class. 
 * Response format json output
 */
export async function short(ip, url, title){ 
    let tit = title ? title : 'client';  
    if(Array.isArray(url) || !url) { 
        throw new ErrorHandler(400, "Please provide a valid url")
    }
    try {
        let symbolid = await createshort();
        let validatedUrl = await urlvalidator(url);

        return await model.create(symbolid.shorturl, validatedUrl, tit, ip, symbolid.id)
    }catch(e){
       throw new ErrorHandler(422, "Provided url is not valid")
    }
}
