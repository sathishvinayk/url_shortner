/**
 * Shorten fn
 * @param {object}doc Response body
 * @Returns {buffer} output
 */
export async function reunion(doc) {
    let data = doc;
    data.createdAt = doc.createdAt.toLocaleString();
    let buffer = await Buffer.from(JSON.stringify(data))

    return buffer
}
