/**
 * Generic Error Handler(Http)
 * @params {number, string} StatusCode and message
 */
export class ErrorHandler extends Error{
    constructor(status, message){
        super(message);
        this.status = status;
    }
}
