import xmlbuilder from 'xmlbuilder';

/**
 * Async cascase_success XML output
 * @param {object}data buffered data
 * @param {object}state Response state object 
 * @returns {xml} doc
 */
async function cascadeIt(data, state){
    let buffer = await JSON.parse(data.toString());
    let doc = xmlbuilder.create('result').
    ele('url').
        ele('keyword').
        txt(buffer.keyword).
        up().
                    ele('url').
        dat(buffer.url).
        up().
                    ele('title').
        txt(buffer.title).
        up().
                    ele('date').
        txt(buffer.createdAt).
        up().
                    ele('ip').
        txt(buffer.ip).
        up().
                up().
                    ele('status').
        txt(state.status).
        up().
                    ele('message').
        txt(state.message).
        up().
                    ele('title').
        txt(buffer.title).
        up().
                    ele('shorturl').
        txt(buffer.shorturl).
        up().
                    ele('statusCode').
        txt(state.statuscode).
        up().
    end({ pretty: true }) 
    
    return doc
}

/**
 * Factory interface for the output format
 * methods json and xml.
 */
export const formatter = {
    json: (state, data, res) => {     
        res.setHeader('content-type', 'application/json'); 
        res.status(state.statuscode)
        let buffer = JSON.parse(data.toString());
        let result = Object.assign(buffer, state)

        return res.json(result)
    },
    xml: async(state, data, res) => {
        res.setHeader('content-type', 'text/xml');
        res.status(state.statuscode)    
        let result = await cascadeIt(data, state)    

        return res.send(result)
    }
}

/**
 * XmlError pass
 * @param {string} message (Message pass)
 * @returns {xml}doc (compiled doc)
 */
export function xmlerror(message) {
    let doc = xmlbuilder.create('result').
        ele('error').
        ele('message').
        txt(message).
        up().
        up().
        end({ pretty: true });
    
    return doc
}
