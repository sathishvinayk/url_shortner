import DBG from 'debug';
import util from 'util';
import mongodb from 'mongodb';
import { reunion, ErrorHandler } from '../helpers';
const { MongoClient } = mongodb;
const debug = DBG('notes:notes-mongodb'); 

/**
 * Global variable for setting the client MongoDB
 */
let client = null;
const MONGO_URI = process.env.NODE_ENV === 'development' ? process.env.MONGO_DEV_URL : process.env.MONGO_PROD_URL;

/**
 * Connection to DB instance 
 * @returns { object } db, client
 */
const connectDB = async function() {
    if(!client) {
        client = await MongoClient.connect(MONGO_URI, { useNewUrlParser: true }); 
    }
    
    return {
        db: client.db(process.env.MONGO_DBNAME),
        client
    };
}

/** 
 * @param {string} id shortid 
 * @param {string} url long url
 * @param {string} title from request
 * @param {string} ip from request
 * @param {string} keyword generated for the url
 * @returns {object} with data
 */
export const create = async function(id, url, title, ip, keyword){
    
    const { db } = await connectDB();
    const collection = db.collection('urldata');
    let doc = await collection.findOne({ url } , { shorturl: id });
    if(!doc){
        let dce = await collection.insertOne({
            ip,
            shorturl: id,
            url,
            title,
            keyword,
            createdAt: new Date()
        });    
        let data = await reunion(dce.ops[0])

        return data
    }
    let data = await reunion(doc)
    
    return data
}

/**
 * urllist for querying id's
 * @params {null}
 * @returns {keyz}
 */
export const urllist = async function () {
    const { db } = await connectDB();
    debug(`urllist ${util.inspect(db)}`);
    const collection = db.collection('urldata');
    const keyz = await new Promise((resolve, reject) => {
        var allkeyz = [];
        collection.find({}).forEach(
            (data) => {
                allkeyz.push(data.shorturl); 
            },
            (err) => {
                if(err) {
                    reject(err);
                } else {
                    resolve(allkeyz); 
                }
            } 
        );
    });
    
return keyz;
}

/**
 * Async read function
 * @param {string} key (Key for the document)
 * @returns {object}
 */
export const read = async function(key){
    const { db } = await connectDB();
    const collection = db.collection('urldata');
    const doc = await collection.findOne({ keyword: key })
    if (!doc) {
        throw new ErrorHandler(404, "Document resource not found")
    }
    
    return {
        ip: doc.ip,
        keyword: doc.keyword,
        url: doc.url,
        title: doc.title,
        shorturl: doc.shorturl
    };
}

/**
 * Destroy fn to remove docs
 * @Param {string} key for the doc
 * @return {null}
 */
export const destroy = async function(key){
    const { db } = await connectDB();
    const collection = db.collection('urldata');
    await collection.findOneAndDelete({ shorturl: key })
}

/**
 * close for winding up db
 * @params {null}
 * @returns {null}
 */
export const close = async function () {
    if(client) {
        await client.close();
        client = null;
    }
}
