import express from 'express';
import { bounce } from '../../handlers'

export const router = express.Router();

/**
 * home fn
 * @param {obj} req parameters
 * @param {obj} res parameters
 * @param {obj} next parameters
 * @Returns {json} status
 */
function home(req,res, next){
  res.json({ "ping": "success" }) 
}

/**
 * Async bounce to original
 * @param {obj}req (Shorten id)
 * @param {obj}res (Redirection to original url)
 * @param {obj}next (next if error)
 * @Returns {url} Redirection
 */
async function bouncer(req, res, next){
  let { id } = req.params;
  try {
    let data = await bounce(id);
    res.status(301);
    res.set('Content-Type', 'text/plain; charset=utf-8');

    return res.redirect(data.url)
  } catch(e){

    return next(e);
  }
}

//Cascading Routes 
router.get('/', home)
router.get('/:id', bouncer)
