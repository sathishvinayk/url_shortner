/**
 * Routes is for the Shorten req and res pass thru!
 */
import express from 'express';
import { short } from '../../handlers';
import { formatter } from '../../helpers';

export const router = express.Router();

/**
 * Global acknowledgement for user response
 */
var DEFAULT_RESPONSE = null;

/**
 * Async shorten
 * @param {object}req (url, title)
 * @param {object}res (response code)
 * @param {object}next (ByPass with status)
 * @Returns {obj} Doc
 */
async function shorten(req,res,next) {
  let ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
  req.body.output === 'json' ? DEFAULT_RESPONSE = 'json' : DEFAULT_RESPONSE = 'xml';  
  try { 
    let data = await short(ip, req.body.url, req.body.title)
    let state = { 
      statuscode: 200,
      status: "success",
      message: "Record successfully added!" 
    }
    
    return formatter[DEFAULT_RESPONSE](state, data, res)
  } catch(e){

    return next(e);
  }
}

//Routes 
router.post('/', shorten)
